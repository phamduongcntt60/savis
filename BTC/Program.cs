﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phanso

{
    class PhanSo
    {
        protected int Mau, Tu;
      
        public void Nhap()
        {
            int a = 0, b = 0;
            Console.Write("\nNhap vao phan so [a/b]:  ");
            NhapThem(ref a, ref b);
            this.Tu = a;
            this.Mau = b;
        }

        public void NhapThem(ref int a, ref int b)
        {
            string so = Console.ReadLine();
            string[] splitStr = so.Split('/');
            a = int.Parse(splitStr[0]);
            b = int.Parse(splitStr[1]);
        }
        public void Gan(int tu, int mau)
        {
            this.Tu = tu;
            this.Mau = mau;
        }

        public void Xuat()
        {
            Console.WriteLine("Phan so la: {0}/{1}", Tu, Mau);

        }

        public void Cong(int a, int b)
        {
            this.Tu = a * Mau + b * Tu;
            this.Mau = b * Mau;
        }
        public PhanSo Tru(PhanSo S1)
        {
                PhanSo ps1 = new PhanSo();
                ps1.Tu = Tu * S1.Mau - Mau * S1.Tu;
                ps1.Mau = Mau * S1.Mau;
                return ps1;
        }
        public PhanSo Nhan(PhanSo S1)
        {
            PhanSo ps1 = new PhanSo();
            ps1.Tu = Tu * S1.Tu;
            ps1.Mau = Mau * S1.Mau;
            return ps1;
        }
      public PhanSo Chia(PhanSo S1)
        {
            PhanSo ps1 = new PhanSo();
            ps1.Tu = Tu * S1.Mau;
            ps1.Mau = Mau * S1.Tu;
            return ps1;
        }

        
    }
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0, b = 0;
            PhanSo ps = new PhanSo();
            tag:
            ps.Nhap();
            ps.Xuat();
            Console.WriteLine("\nXin moi chon chuc nang: ");
            Console.WriteLine("Bam 1 de cong phan so[a/b]");
            Console.WriteLine("Bam 2 de tru phan so[a/b]");
            Console.WriteLine("Bam 3 de nhan phan so[a/b]");
            Console.WriteLine("Bam 4 de chia phan so[a/b]");
            Console.WriteLine("Bam 5 Exit.");
            int choose = int.Parse(Console.ReadLine());
            switch (choose)
            {
                case 1:
                    Console.Write("\nNhap vao phan so: ");
                    ps.NhapThem(ref a, ref b);
                    ps.Cong(a, b);
                    ps.Xuat();
                    goto tag;
                    
                case 2:
                    Console.Write("\nNhap vao phan so: ");
                    PhanSo ps1 = new PhanSo();
                    ps1.Nhap();
                    ps = ps.Tru(ps1);
                    ps.Xuat();
                    goto tag;
                    
                case 3:
                    Console.Write("\nNhap vao phan so: ");
                    PhanSo ps2 = new PhanSo();
                    ps2.Nhap();
                    ps = ps.Nhan(ps2);
                    ps.Xuat();
                    goto tag;
                   
                case 4:
                    Console.Write("\nNhap vao phan so: ");
                    PhanSo ps3 = new PhanSo();
                    ps3.Nhap();
                    ps = ps.Chia(ps3);
                    ps.Xuat();
                    goto tag;
                  
                case 5:
                    break;
            }


        }
    }
}
